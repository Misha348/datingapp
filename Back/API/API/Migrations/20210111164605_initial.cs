﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace API.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserName = table.Column<string>(type: "TEXT", nullable: true),
                    DateCreate = table.Column<DateTime>(type: "TEXT", nullable: true),
                    DateModify = table.Column<DateTime>(type: "TEXT", nullable: true),
                    DateDelete = table.Column<DateTime>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "DateCreate", "DateDelete", "DateModify", "UserName" },
                values: new object[] { 1, new DateTime(2021, 1, 11, 18, 46, 4, 699, DateTimeKind.Local).AddTicks(9520), null, null, "Bill" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "DateCreate", "DateDelete", "DateModify", "UserName" },
                values: new object[] { 2, new DateTime(2021, 1, 11, 18, 46, 4, 704, DateTimeKind.Local).AddTicks(3832), null, null, "Bill" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "DateCreate", "DateDelete", "DateModify", "UserName" },
                values: new object[] { 3, new DateTime(2021, 1, 11, 18, 46, 4, 704, DateTimeKind.Local).AddTicks(3889), null, null, "Bill" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
