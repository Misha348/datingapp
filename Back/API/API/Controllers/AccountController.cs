﻿using API.Data;
using API.DTOModels;
using API.Entities;
using API.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace API.Controllers
{
	public class AccountController : BaseApiController
	{
		private readonly DataContext _context;
		private readonly ITokenService _tokenService;
		public AccountController(DataContext context, ITokenService tokenSrvice)
		{
			_context = context;
			_tokenService = tokenSrvice;
		}

		[HttpPost("register")]
		public async Task<ActionResult<UserDTO>> Register(RegisterDTO registerDTO)
		{
			if (await UserExist(registerDTO.UserName))
				return BadRequest("User name is already taken");

			using var hmac = new HMACSHA512();
			var user = new AppUser
			{
				UserName = registerDTO.UserName,
				PasswordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(registerDTO.Password)), PasswordSalt = hmac.Key,
				DateCreate = DateTime.Now.ToLocalTime()
			};
			_context.Add(user);
			await _context.SaveChangesAsync();

			return new UserDTO
			{
				UserName = user.UserName,
				Token = _tokenService.CreateToken(user)
			};
		}

		[HttpPost("login")]
		public async Task<ActionResult<UserDTO>> Login(LoginDTO loginDTO)
		{
			var user = await _context.Users.SingleOrDefaultAsync(u => u.UserName == loginDTO.UserName);

			if (user == null)
				return Unauthorized("invalid user name");

			using var hmac = new HMACSHA512(user.PasswordSalt);
			var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(loginDTO.Password));
			
			for (int i = 0; i < computedHash.Length; i++)
			{
				if (computedHash[i] != user.PasswordHash[i])
					return Unauthorized("invalid password");
			}

			return new UserDTO
			{
				UserName = user.UserName,
				Token = _tokenService.CreateToken(user)
			};
		}
	

		private async Task<bool> UserExist(string userName)
		{
			return await _context.Users.AnyAsync(x => x.UserName == userName/*.ToLower()*/);
		}
	}
}

